const mongoose = require("mongoose");
const url = "mongodb://127.0.0.1:27017/amediatv";
const connectDB = async() => {
    try {
        const conn = await mongoose.connect(url, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });
        console.log(`MongoDB connected : ${conn.connection.host}`);
    } catch (err) {
        console.log(err);
    }
};
module.exports = connectDB;